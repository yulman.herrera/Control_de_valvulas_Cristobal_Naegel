#include <ESP8266WiFi.h>
#include <Stream.h>
#include <PubSubClient.h>
#include <string.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h> 
#include "WiFiManager.h"  
#include <Ticker.h> //LED blink
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <ESP8266WiFiMulti.h> //Libreria para OTA
#include <ESP8266HTTPClient.h> //Libreria para OTA
#include <ESP8266httpUpdate.h> //Libreria para OTA
#include "RTClib.h"
#include <EEPROM.h>



LiquidCrystal_I2C lcd(0x3F,20,4); 

RTC_DS3231 rtc; //lineas necesarias para el RTC
char daysOfTheWeek[7][12] = {"7", "1", "2", "3", "4", "5", "6"}; //lineas para uso de funciones en RTC
DateTime now = rtc.now();

#define modelNumber  "TecAnd0002"
#define serialNumber "jR8Dk6" //VALVULA 1 Tarjeta grande
//#define serialNumber "Wa82UI" //VALVULA 2 Tarjeta pequeña
#define statusValve1 45
#define statusValve2 46
const char* mqttID = serialNumber; //Equivalente a Número de Serie de modelNumber
#define slash "/"
int steps=0;
const char* _ssid = "tecnoandina-IoT";
const char* wifiPassword = "kennedy5600";
boolean flag_open=1,flag_close=1;
unsigned long open_millis, close_millis;

//************************************************************************
//****************TOPICS**************************************************
//************************************************************************


String _main = modelNumber slash serialNumber "/"; 
String statusInput= _main+"input/status";
String statusOutput= _main+"output/status";
String status= _main+"status";
String valvula= _main+"input/valvula";
String voltaje=_main+"output/batt_voltaje";
String LCD= _main+"input/LCD";
String OTA=_main+"input/OTAUpdate";
String riego=_main+"input/riego";//    TecAnd0002/jR8Dk6/input/riego
String e2prom=_main+"input/eeprom";
String riego_borrar=_main+"input/riego_borrar";


//************************************************************************
//****************TOPICS**************************************************
//************************************************************************

#define PWMB D8
#define BIN2 D7
#define BIN1 D6
#define STBY D5
#define AIN1 D4
#define AIN2 D3
#define PWMA D0


/****************************************************************/
/*  ****************WiFiManager**********************************/

//LED Blink
Ticker ticker;


void tick(){
    //toggle state
    int state = digitalRead(BUILTIN_LED);  // get the current state of GPIO1 pin
    digitalWrite(BUILTIN_LED, !state);     // set pin to the opposite state
}
/******************************************************/
/* WiFiManager configModeCallback *********************/
void configModeCallback (WiFiManager *myWiFiManager) {
    Serial.println("Entered config mode");
    Serial.println(WiFi.softAPIP());
    //if you used auto generated SSID, print it
    Serial.println(myWiFiManager->getConfigPortalSSID());
    //entered config mode, make led toggle faster
    ticker.attach(0.2, tick);
  }

/******************************************************/
/* LED BLink Tick *************************************/

/*  ****************WiFiManager************************/
/******************************************************/


#define mqttPI true

#ifdef mqttPI
const char* mqttServer = "tecnoandina-server.ddns.net";
//const char* mqttServer = "iot.eclipse.org";
const int mqttPort = 1883;
const char* mqttUser = "tecnoandina";
const char* mqttPassWd = "kennedy5600";
#endif

WiFiClient espClient002;
PubSubClient client(espClient002);
ESP8266WiFiMulti WiFiMulti; //Libreria para OTA


//**********************************************CALLBACK**************************************************************
void callback(char* topic, byte* payload, unsigned int length) {


const char len=length;
char mensaje[len];
String TOPic= topic;
String payld= (char*) payload;

if(TOPic==statusInput)
{
String valvulaUno, valvulaDos;
  
  for (int i = 0; i < length;  i++)
 {
 mensaje[i]=(char)payload[i];
 }

payld=mensaje;
payld=payld.substring(0,length);

if (payld=="1")
{
  valvulaUno=EEPROM.read(statusValve1);

  if(valvulaUno=="1")
  {
client.publish(statusOutput.c_str(),"on");
  }
  if(valvulaUno=="0")
  {
client.publish(statusOutput.c_str(),"off");
  }
  
}

if(payld=="2")
{
 valvulaDos=EEPROM.read(statusValve2);
 
  if(valvulaDos=="1")
  {
client.publish(statusOutput.c_str(),"on");
  }
  if(valvulaDos=="0")
  {
client.publish(statusOutput.c_str(),"off");
  }
}
  
}



if(TOPic==LCD)
{

for (int i = 0; i < length;  i++)
 {
 mensaje[i]=(char)payload[i];
 }

 if(!(strncmp(mensaje,"on",2)))
  {
   
   lcd.backlight();

  }

  if(!(strncmp(mensaje,"off",3)))
  { 
  

  lcd.noBacklight();
  
  }

}


if(TOPic==valvula)
 {

 Serial.println("entrando a valvula");
String state;
int valve, pos_coma;
for (int i = 0; i < length;  i++)
 {
 mensaje[i]=(char)payload[i];
 }

payld=mensaje;
payld=payld.substring(0,length);
pos_coma= payld.indexOf(',');
Serial.println(payld.c_str());
valve=atoi(payld.substring(0,pos_coma).c_str());
state= payld.substring(pos_coma+1,payld.length());
Serial.println(state.c_str());
Serial.println(valve);

 if(valve==1)
  {
  if(state=="on")
  {
     open_valv_1();
  }
  if(state=="off")
  {
     close_valv_1();
     
  }
  }


 if(valve==2)
  {
  if(state=="on")
  {
     open_valv_2();
  }
  if(state=="off")
  {
     close_valv_2();
     
  }
  }



}

 

if(TOPic==e2prom)
{
for(int i=0;i<50;i++)
{
  Serial.print("eeprom posicion ");
  Serial.print(i);
  Serial.print(": ");
  Serial.println(EEPROM.read(i));
  
}
}

if(TOPic==riego)
{
 

int dia,hora_ad, min_ad, hora_cd, min_cd, pos_coma, valv; 

for (int i = 0; i < length;  i++)
 {
 mensaje[i]=(char)payload[i];
 }
payld=mensaje;
payld=payld.substring(0,length);
Serial.println(payld.c_str());

//grabacion eeprom dia de riego*******************
pos_coma= payld.indexOf(',');
dia=atoi(payld.substring(0,pos_coma).c_str());
//Serial.println(dia);
payld= payld.substring(pos_coma+1,payld.length());
EEPROM.write(dia,1);
EEPROM.commit();
Serial.print("Dia_eeprom= ");
Serial.println(EEPROM.read(dia));


//grabacion eeprom hora apertura de riego*******************
pos_coma= payld.indexOf(',');
hora_ad=atoi(payld.substring(0,pos_coma).c_str());
//Serial.println(hora_ad);
payld= payld.substring(pos_coma+1,payld.length());
EEPROM.write(dia+7,hora_ad);
EEPROM.commit();
Serial.print("Hora_apertura_dia= ");
Serial.println(EEPROM.read(dia+7));


//grabacion eeprom minuto apertura de riego*******************
pos_coma= payld.indexOf(',');
min_ad=atoi(payld.substring(0,pos_coma).c_str());
//Serial.println(hora_cd);
payld= payld.substring(pos_coma+1,payld.length());
EEPROM.write(dia+14,min_ad);
EEPROM.commit();
Serial.print("minuto_apertura_dia= ");
Serial.println(EEPROM.read(dia+14));


//grabacion eeprom hora cierre de riego*******************
pos_coma= payld.indexOf(',');
hora_cd=atoi(payld.substring(0,pos_coma).c_str());
//Serial.println(hora_cd);
payld= payld.substring(pos_coma+1,payld.length());
EEPROM.write(dia+21,hora_cd);
EEPROM.commit();
Serial.print("Hora_cierre_dia= ");
Serial.println(EEPROM.read(dia+21));


//grabacion eeprom minuto cierre de riego*******************
pos_coma= payld.indexOf(',');
min_cd=atoi(payld.substring(0,pos_coma).c_str());
//Serial.println(hora_cd);
payld= payld.substring(pos_coma+1,payld.length());
EEPROM.write(dia+28,min_cd);
EEPROM.commit();
Serial.print("minuto_cierre_dia= ");
Serial.println(EEPROM.read(dia+28));


pos_coma= payld.indexOf(',');
valv=atoi(payld.substring(0,pos_coma).c_str());
//Serial.println(valv);
EEPROM.write(dia+35,valv);
EEPROM.commit();
Serial.print("valvula= ");
Serial.println(EEPROM.read(dia+35));
Serial.println();


}


if(TOPic==riego_borrar)
{
  Serial.println("borrando");

int pos_coma,dia;
for (int i = 0; i < length;  i++)
 {
 mensaje[i]=(char)payload[i];
 }
payld=mensaje;
payld=payld.substring(0,length);
Serial.println(payld.c_str());

pos_coma= payld.indexOf(',');
dia=atoi(payld.substring(0,pos_coma).c_str());

for(int i=dia;i<50;i++)
{

  EEPROM.write(i,0);
  EEPROM.commit();
 i+=6;
}

}

//Instrucción de buscar un nombre de archivo para actualizar el firmware (OTA)***************************
  if(strcmp(topic,modelNumber "/" serialNumber "/input/OTAUpdate")==0){
    Serial.println("ingresando al OTA");
    payload[length] ='\0';
    String fileName = "/OTA/";
    fileName += String((char*)payload);
    Serial.println("Checking for firmware Update");
    if((WiFiMulti.run() == WL_CONNECTED)) {
      t_httpUpdate_return ret = ESPhttpUpdate.update("tecnoandina.eastus.cloudapp.azure.com",80,fileName);
      switch(ret) {
          case HTTP_UPDATE_FAILED:
              //Serial.printf("HTTP_UPDATE_FAILED Error (%d): %s", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
              Serial.println();
              client.publish(modelNumber "/" serialNumber "/output/OTAUpdate", "Update Failed");
              break;
  
          case HTTP_UPDATE_NO_UPDATES:
              Serial.println("HTTP_UPDATE_NO_UPDATES");
              client.publish(modelNumber "/" serialNumber "/output/OTAUpdate", "No Update");
              break;
  
          case HTTP_UPDATE_OK:
              Serial.println("HTTP_UPDATE_OK");
              break;
         
      }//End Switch

    }//End WiFiMulti.run()
  }//End for topic if
}//*************************************************************END CALLBACK

//**********************************************WIFI**************************************************************
void wifi_connect(const char* ssid, const char* wifiPassWd)
{

//********************LOOP WIFI******************************
//***********************************************************
 Serial.println("Connecting to Wi-Fi...");
 WiFi.begin(ssid,wifiPassWd);
 while (WiFi.status() != WL_CONNECTED) {
 delay(500);
 Serial.print("Connecting to Wifi ");
 Serial.println(ssid);
 }
 Serial.println("Connected to WiFi network");
 Serial.println(WiFi.localIP());
}


//**********************************************MQTT**************************************************************

void mqtt_connect()
{
  //********************LOOP MQTT******************************
//***********************************************************
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
  while (!client.connected()) {
    Serial.print("Connecting to MQTT ");
    Serial.println(mqttServer);

if (client.connect(mqttID,mqttUser,mqttPassWd,status.c_str(),1,1,"OFFLINE")) {//**
  client.publish(status.c_str(),"ONLINE",true);
      Serial.println("Connected to MQTT Broker");
      Serial.println("----------------------------------------");
    }//End If
    else {
      Serial.print("failed with state: ");
      Serial.println(client.state());
      delay(2000);
    }//End Else

client.subscribe(statusInput.c_str());
client.subscribe(valvula.c_str());
client.subscribe(OTA.c_str());
client.subscribe(LCD.c_str());
client.subscribe(riego.c_str());
//client.subscribe(e2prom.c_str());
client.subscribe(riego_borrar.c_str());

}//END While
}


//**********************************************RECONNECT**************************************************************
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
   
   if (client.connect(mqttID,mqttUser,mqttPassWd,status.c_str(),1,1,"OFFLINE")) { 
client.publish(status.c_str(),"ONLINE",true);

client.subscribe(statusInput.c_str());
client.subscribe(valvula.c_str());
client.subscribe(OTA.c_str());
client.subscribe(LCD.c_str());
client.subscribe(riego.c_str());
//client.subscribe(e2prom.c_str());
client.subscribe(riego_borrar.c_str());



    } 
      delay(500);
    }
  }


//**********************************************MEDICION*******************************************
void medicion()
{
  String tgraph="testingHardware,pruebas=baterias voltaje=";
  steps++;
 // Serial.println(steps);
int f=3;
char _voltaje[10];
float a = analogRead(0);
  float b = (a*13.3)/863;  //Tarjeta grande Para medir hasta 15V ---       valvula 1
 // float b = (a*12.41)/871; // Tarjeta pequeña Para medir hasta 15V -- vavula 2


  String k= (String)b;
  lcd.setCursor(0,f);
  lcd.print("Batt_volt:");
  lcd.setCursor(10,f);
  lcd.print(String(b).c_str());
  delay(500);

if(steps>120)
{
  tgraph+=String(b);

 // client.publish("TecAnd0002/jR8Dk6/output/batt_voltaje",tgraph.c_str());
  client.publish(voltaje.c_str(),tgraph.c_str());

    steps=0;

}

}

String gettime()
{
 DateTime now = rtc.now();
  String tiempo;
  tiempo= (String)now.hour();
  tiempo+=":";
 if(now.minute()<10)
  {
    tiempo=tiempo+"0"+(String)now.minute();
  }
  else
  {
    tiempo+=(String)now.minute();
  }
  
  tiempo+=":";

  if(now.second()<10)
  {
    tiempo=tiempo+"0"+(String)now.second();
  }
  else
  {
    tiempo+=(String)now.second();
  }
  
 
  lcd.setCursor(7,2);
  lcd.print(tiempo.c_str());
  //Serial.println(tiempo.c_str());

  return tiempo;
}

  void setup() {


String valvulaUno,valvulaDos;
  Serial.begin(9600);


EEPROM.begin(50);//*********************************************************************************************************************************************************
//for(int i=0;i<30;i++)
//{
//  EEPROM.write(i,0);
//}
 //==================================================
    //Copiar para usar WiFiManager
    //set led pin as output
    pinMode(BUILTIN_LED, OUTPUT);
    // start ticker with 0.5 because we start in AP mode and try to connect
    ticker.attach(0.6, tick);

    //WiFiManager
    //Local intialization. Once its business is done, there is no need to keep it around
    WiFiManager wifiManager;
    //reset settings - for testing
    //wifiManager.resetSettings();

    //set callback that gets called when connecting to previous WiFi fails, and enters Access Point mode
    wifiManager.setAPCallback(configModeCallback);

    //fetches ssid and pass and tries to connect
    //if it does not connect it starts an access point with the specified name
    //here  "AutoConnectAP"
    //and goes into a blocking loop awaiting configuration
    if(!wifiManager.autoConnect(serialNumber " TECNOANDINA")) {
        Serial.println("failed to connect and hit timeout");
        //reset and try again, or maybe put it to deep sleep
        ESP.reset();
        delay(1000);
    }  //end if

    //if you get here you have connected to the WiFi
    Serial.println("connected to WiFi");
    ticker.detach();
    //keep LED on
    digitalWrite(BUILTIN_LED, LOW);
    //==================================================

valvulaUno=EEPROM.read(statusValve1);
valvulaDos=EEPROM.read(statusValve2);
    
  pinMode(AIN2, OUTPUT);
  pinMode(AIN1, OUTPUT);
  pinMode(STBY, OUTPUT);
  pinMode(PWMA, OUTPUT);
  pinMode(BIN2, OUTPUT);
  pinMode(BIN1, OUTPUT);
  pinMode(PWMB, OUTPUT);


mqtt_connect();


  lcd.init();  



lcd.setCursor(0,0);
lcd.print("valv_1:");
lcd.setCursor(0,1);
lcd.print("valv_2:");
lcd.setCursor(0,2);
lcd.print("Tiempo:");


  if(valvulaUno=="1")
  {
     lcd.setCursor(9,0);
     lcd.print("abierta");
  }

if(valvulaUno=="0")
  {
     lcd.setCursor(9,0);
     lcd.print("cerrada");
  }


 if(valvulaDos=="1")
  {
     lcd.setCursor(9,1);
     lcd.print("abierta");
  }

if(valvulaDos=="0")
  {
     lcd.setCursor(9,1);
     lcd.print("cerrada");
  }



//=======================Lineas para el RTC====================================
#ifndef ESP8266
  while (!Serial); // for Leonardo/Micro/Zero
#endif
  delay(3000); // wait for console opening
  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }
  if (rtc.lostPower()) {
    Serial.println("RTC lost power, lets set the time!");
  //rtc.adjust(DateTime(2018, 1, 5, 13, 25, 00)); // con esta linea se setea la fecha y la hora
  }
//=======================Lineas para el RTC====================================


}


void progRiego()
{
  //Serial.println("riego");
  DateTime now = rtc.now();
  int valDay=  atoi(daysOfTheWeek[now.dayOfTheWeek()]);


if(flag_open==0 && millis()-open_millis>80000)
{
  flag_open=1;
}

if(flag_close==0 && millis()-close_millis>80000)
{
  flag_close=1;
}



if(flag_open==1)
{

if(EEPROM.read(valDay)==1 && EEPROM.read(7+valDay)==now.hour() && EEPROM.read(14+valDay)==now.minute()) //abrir valvula
{
  if(EEPROM.read(35+valDay)==1) //abrir valvula 1
  {
     open_valv_1();
     //Serial.println("abriendo valvula 1");
  }
  
  if(EEPROM.read(35+valDay)==2) //abrir valvula 2
  {
     open_valv_2();
     //Serial.println("abriendo valvula 2");
  }
  flag_open=0;
  open_millis= millis();
}
}


if(flag_close==1)
{

if(EEPROM.read(valDay)==1 && EEPROM.read(21+valDay)==now.hour() && EEPROM.read(28+valDay)==now.minute()) //cerrar valvula
//if(EEPROM.read(valDay)==1 && EEPROM.read(21+valDay)==now.hour() && EEPROM.read(28+valDay)==now.minute()) //cerrar valvula
{
  if(EEPROM.read(35+valDay)==1) //cerrar valvula 1
  {
     close_valv_1();
     //Serial.println("cerrando valvula 1");
  }
  
  if(EEPROM.read(35+valDay)==2) //cerrar valvula 2
  {
     close_valv_2();
     //Serial.println("cerrando valvula 2");
  }
  flag_close=0;
  close_millis= millis();
}
}

}

void open_valv_1()
{
     //apertura de valvula latch A 
     //(voltaje positivo segun color del cable del motor)
     digitalWrite(PWMA, HIGH); 
     digitalWrite(STBY, HIGH); 
     digitalWrite(AIN1, HIGH);        
     digitalWrite(AIN2, LOW);
   //  Serial.println("valvula 1 abierta");
     delay(400); 
     digitalWrite(STBY, LOW);
     digitalWrite(PWMA, LOW);
     digitalWrite(AIN1, LOW);
     lcd.setCursor(9,0);
     lcd.print("abierta");
     EEPROM.write(statusValve1,1);
     EEPROM.commit();
}
void close_valv_1()
{
     //cierre de valvula latch A 
     //(voltaje negativo segun color del cable del motor)
     digitalWrite(PWMA, HIGH); 
     digitalWrite(STBY, HIGH); 
     digitalWrite(AIN2, HIGH);        
     digitalWrite(AIN1, LOW);
    // Serial.println("valvula 1 cerrada");
     delay(400); 
     digitalWrite(STBY, LOW);
     digitalWrite(PWMA, LOW);
     digitalWrite(AIN2, LOW);
     lcd.setCursor(9,0);
     lcd.print("cerrada"); 
     EEPROM.write(statusValve1,0);
     EEPROM.commit();
}
void open_valv_2()
{
  digitalWrite(PWMB, HIGH); 
  digitalWrite(STBY, HIGH); 
  digitalWrite(BIN1, HIGH);        
  digitalWrite(BIN2, LOW);
 // Serial.println("valvula 2 abierta");
  delay(400); 
  digitalWrite(STBY, LOW);
  digitalWrite(PWMB, LOW);
  digitalWrite(BIN1, LOW);
  lcd.setCursor(9,1);
  lcd.print("abierta");
  EEPROM.write(statusValve2,1);
     EEPROM.commit();
}
void close_valv_2()
{
   digitalWrite(PWMB, HIGH); 
   digitalWrite(STBY, HIGH); 
   digitalWrite(BIN2, HIGH);        
   digitalWrite(BIN1, LOW);
  // Serial.println("valvula 2 cerrada");
   delay(400); 
   digitalWrite(STBY, LOW);
   digitalWrite(PWMB, LOW);
   digitalWrite(BIN2, LOW);
   lcd.setCursor(9,1);
   lcd.print("cerrada");
   EEPROM.write(statusValve2,0);
     EEPROM.commit();
}


void loop() {

medicion();
gettime();
progRiego();


if (!client.connected())
{
reconnect();
}
client.loop();


}